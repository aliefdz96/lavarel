@extends('layout.master')

@section('judul')
HALAMAN HOME
@endsection
    
@section('content')
<h1>MEDIA ONLINE</h1>
<h2>Social Media Developer</h2>

    <p>Belajar dan Berbagi agar hidup menjadi lebih baik</p>

    <h2>Benefit Join di Media Online</h2>

    <ul>
        <li>Mendapatkan motivasi dari sesama Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web Developer terbaik</li>
    </ul>

    <h2>Cara Bergabung ke Media Online</h2>

    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection
    