@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
  <h1>Buat Akun Baru</h1>
  <h3>Sign Up Form</h3>
  <form action="/welcome" method="POST">
    @csrf
    <label for="first">First Name</label><br><br>
    <input type="text" name="first"><br><br>
    
    <label for="last">Last Name</label><br><br>
    <input type="text" name="last"><br><br>
    
    <label>Password:</label><br>
    <input type="password" name="pass"><br><br>

    <label for="gender">Gender:</label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>
    
    <label for="nationality">Nationality:</label><br><br>
    <select name="nationality" id="">
      <option value="indonesia">Indonesia</option>
      <option value="english">Foreginers</option>
    </select><br><br>
    
    <label for="language">Language Spoken:</label><br><br>
    <input type="checkbox">Bahasa Indonesia <br>
    <input type="checkbox">English <br>
    <input type="checkbox">Other <br><br>
    
    <label for="bio">Bio:</label><br><br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>
    
    <input type="submit">
  </form>
@endsection